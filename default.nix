let
  pkgsSrc = fetchTarball {
    # As of 2022-07-19
    url = "https://github.com/NixOS/nixpkgs/archive/d2db10786f27619d5519b12b03fb10dc8ca95e59.tar.gz";
    sha256 = "0s9gigs3ylnq5b94rfcmxvrmmr3kzhs497gksajf638d5bv7zcl5";
  };
  pkgs = import pkgsSrc { };
in
  pkgs.dockerTools.buildImage {
    name = "superboum/nixtainer";
    extraCommands = ''
      mkdir -p tmp
    '';
    copyToRoot = pkgs.buildEnv {
      name = "image-root";
      pathsToLink = [ "/bin" ];
      paths = [
        pkgs.nixStatic
        pkgs.pkgsStatic.busybox
      ];
    };
  }
