# A lightweight Nix environment in a docker container

This container is inteded to be used in a CI environment,
and especially with Drone CI.

Build with:

```
docker load < $(nix-build)
```

## Notes

Drone CI hardcodes the shell with `/bin/sh`,
so whatever the change you make to the container, 
you must ensure that `/bin/sh` exists and works.

